package lib;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Описнаие участника мероприятия
 */
public abstract class Human implements DoHuman{

    private Outfit outfit;
    private Feel feel;
    private final String name;

    public Human(String name, Outfit outfit, Feel feel) {
        this(name);
        setDress(outfit);
        setFeel(feel);
    }
    public Human(String name, Outfit outfit) {
        this(name);
        setDress(outfit);
    }
    public Human(String name) {
        this.name = name;
        this.outfit = new Outfit("Default");
        this.feel = Feel.NULL;
    }

    @Override
    public String toString() { return "Гость " + name; }
    @Override
    public int hashCode() {
        int sum = toString().hashCode() + outfit.hashCode();
        if(feel == Feel.CONSTRAINT) sum += 5*101;
        else if(feel == Feel.NULL) sum += 2*101;
        else if(feel == Feel.OK) sum += 3*101;

        return sum;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj != null)
            return obj.hashCode() == this.hashCode();
        return false;
    }
    /**
     * Установаить наряд на это мероприятие
     * @param outfit
     */
    public void setDress(Outfit outfit){ this.outfit = outfit; }
    /**
     * Установить, как себя чувствовать
     * @param feel
     */
    public void setFeel(Feel feel){ this.feel = feel; }
    /**
     * Как себя чувствует
     * @return
     */
    public Feel getFeel() { return feel; }
    /**
     * Одетый наряд
     * @return
     */
    public Outfit getOutfit() { return outfit; }
    /**
     * Имя гостя
     * @return
     */
    public String getName(){ return name; }
    /**
     * Оченить наряд другого участника
     * @param other
     */
    public abstract void rateOutfit(Human other);

    public static <E> List<E> pickNRandomElements(List<E> list, int n, Random r) {
        int length = list.size();
        if (length < n) return null;
        for (int i = length - 1; i >= length - n; --i)
		 Collections.swap(list, i , r.nextInt(i + 1));
        return list.subList(length - n, length);
    }
    public static <E> List<E> pickNRandomElements(List<E> list, int n) {
        return pickNRandomElements(list, n, ThreadLocalRandom.current());
    }
}
