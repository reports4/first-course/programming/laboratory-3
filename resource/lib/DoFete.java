package lib;

import java.util.ArrayList;

public interface DoFete {
    /**
     * Добавить участника на мероприятие
     * @param h
     */
    void addParticipant(Human h);
    /**
     * Уйти с мероприятия
     * @param h
     */
    void leaveParticipant(Human h);
    /**
     * Начать мероприятие
     */
    void startFete();
    /**
     * Закончить мероприятие
     */
    void stopFate();
    /**
     * Добавить множество гостей на мероприятие
     * @param hs
     */
    default void addParticipants(Human... hs){
        for (Human people: hs) addParticipant(people);
    }
    default void leaveParticipants(ArrayList<Human> guest){
        for(int i = guest.size()-1; i > -1 ; i--) 
            this.leaveParticipant(guest.get(i));
    }
}
