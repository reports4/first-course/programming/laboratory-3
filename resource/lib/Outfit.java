package lib;

/**
 * Описание наряда на участнике
 */
public class Outfit {

    private final char quality;
    private final char evaluation;
    private final String name;

    public Outfit(String name) {
        quality = 'a';
        evaluation = '1';
        this.name = name;
    }
    public Outfit(String name, int quality, int evaluation) {
        this.name = name;
        this.quality = (char)Math.abs(quality);
        this.evaluation = (char)Math.abs(evaluation);
    }

    /**
     * Получить качество наряда
     * @return
     */
    public int getQuality() { return quality; }
    /**
     * Получить оценку наряду
     * @return
     */
    public int getEvaluation() { return evaluation; }
    @Override
    public boolean equals(Object obj) { return this.hashCode() == obj.hashCode(); }
    @Override
    public int hashCode() { return (int)evaluation * 31 + (int) quality * 51; }
    @Override
    public String toString() { return "Одетый наряд" + name; }
}
