package lib;

import java.util.List;
import java.util.Random;

public class Thd extends Thread {
    private Fete fete;
    private boolean work = false;

    public Thd(Fete fate) { this.fete = fate; }
    @Override
    public void run() {
        while (fete.isStartFete() && work) {
            List<Human> getRandomPeople = Human.<Human>pickNRandomElements(fete.getGuest(), 2);
            Human h1 = getRandomPeople.get(0);
            Human h2 = getRandomPeople.get(1);
            int rnadomDo = new Random().nextInt(4) + 1;
            switch (rnadomDo) {
                case 1:
                    h1.great(h2);
                    break;
                case 2:
                    h1.bow_out(h2);
                    break;
                case 3:
                    Phrases phr = Phrases.OTHER;
                    int phrInt = new Random().nextInt(3);
                    switch (phrInt) {
                        case 0:
                            if (fete.getSituation() != Situation.RAIN) phr = Phrases.FEELGOOD;
                            break;
                        case 1:
                            phr = Phrases.FIND;
                            break;
                        case 2:
                            phr = Phrases.OTHER;
                            break;
                    }
                    h1.repeat(h2, phr);
                    break;
                case 4:
                    h1.rateOutfit(h2);
                    break;
            }
            try {
                Thread.sleep(800);
            } catch (InterruptedException e) {
                System.out.println("!!!!<<Error thd>>");
                continue;
            }
        }
    }
    @Override
    public synchronized void start() { work = true; super.start(); }
    @Override
    public void interrupt() { work = false; }
}
