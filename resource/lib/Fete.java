package lib;

import java.util.ArrayList;
import java.util.List;

/**
 * Описание Мероприятие
 */
public abstract class Fete implements DoFete{

    protected ArrayList<Human> guests = new ArrayList<>();
    private Situation situation;
    private boolean startFete = false;
    protected Thd thread;

    public Fete(Situation situation, Human ... guests) {
        this(situation);
        addParticipants(guests);
    }
    public Fete(Situation situation) {
        this.situation = situation;
        thread = new Thd(this);
    }

    /**
     * Получить окружающую обстановку
     * @return
     */
    public Situation getSituation(){ return situation; }
    public void setSituation(Situation s){
        situation = s;
        System.out.println(">>Окружающая обстановка изменилась на " + s);
        if(startFete && s != Situation.RAIN){
            List<Human> getRandomPeople = Human.<Human>pickNRandomElements(guests, 2);
            getRandomPeople.get(0).repeat(getRandomPeople.get(1), Phrases.FEELGOOD);
        }
    }
    /**
     * Началось ли мероприятие
     * @return
     */
    public boolean isStartFete() { return startFete; }
    /**
     * Установить начаоь ли мероприятие
     * @param startFete
     */
    public void setStartFete(boolean startFete) { this.startFete = startFete; }
    public int sizeGuest(){ return guests.size(); }
    public ArrayList<Human> getGuest(){ return guests; }
}
