package lib;

public interface DoHuman {
    /**
     * Поприветсвовать другого участника
     * @param other
     */
    void great(Human other);
    /**
     * Раскланиваться с другим участником
     * @param other
     */
    void bow_out(Human other);
    /**
     * Повторить особенную фразу другому участнику
     * @param phrase
     */
    default void repeat(Human other, Phrases phrase){
        System.out.println(">" + this + " обращается к " + other.toString() + " и говорить: " + phrase.getPhrases());
    }
}
