package lib;
/**
 * Чувство на мероприятии
 */
public enum Feel {
    CONSTRAINT, //Чувствовать себя Стесненно
    OK, //Чувстовать себя Хорошо
    NULL; //Чувствовать себя Никак
}
