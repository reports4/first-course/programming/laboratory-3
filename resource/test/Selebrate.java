package test;

import lib.Fete;
import lib.Human;
import lib.Situation;

public class Selebrate extends Fete {

    public Selebrate(Situation situation, Human ... guests) { super(situation, guests); }
    public Selebrate(Situation situation) { super(situation); }

    @Override
    public void addParticipant(Human h) {
        System.out.println(">" + h + " приходит на мероприяте");
        this.guests.add(h);
    }
    @Override
    public void leaveParticipant(Human h) {
        System.out.println(">" + h + " покидает мероприяте");
        this.guests.remove(h);
    }
    @Override
    public void startFete() {
        System.out.println(">>Начинаем мероприятие");
        this.setStartFete(true);
        thread.start();
    }
    @Override
    public void stopFate() {
        System.out.println(">>Заканчиваем мероприятие");
        this.setStartFete(false);
        thread.interrupt();
        this.leaveParticipants(this.guests);
    }
}
