package test;

import lib.Feel;
import lib.Outfit;
import lib.Situation;

public class Main {
    public static void main(String[] args) throws Exception {
        Guest guest1 = new Guest("Номер1");
        Guest guest2 = new Guest("Номер2", new Outfit("Наряд2", 85, 33));
        Guest guest3 = new Guest("Номер3", new Outfit("Наряд2", 90, 58), Feel.OK);
        Selebrate selebrate = new Selebrate(Situation.RAIN);
        selebrate.addParticipants(guest1, guest2);
        selebrate.startFete();
        selebrate.addParticipant(guest3);
        Thread.sleep(2000);
        selebrate.setSituation(Situation.SUN); //при изменении, реагировали гости
        Thread.sleep(10000);
        selebrate.stopFate();
    }
}
